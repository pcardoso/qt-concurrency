#ifndef COUNTDOWN_H
#define COUNTDOWN_H

#include <QThread>
#include <QDebug>

class Countdown : public QThread{
private:
    int startAt;
public:
     Countdown(int n): QThread(){
         this->startAt = n;
     };
protected:
     void run(){
        int counter = startAt;
         while (counter){
             qInfo("[%d]: %d", this->currentThreadId(), counter--);
             QThread::sleep(1);
         }
         qInfo("[%d]: Over and out!", this->currentThreadId);
     }
};

#endif // COUNTDOWN_H
