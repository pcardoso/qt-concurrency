#include <QThread>
#include <QMutex>
#include <QDebug>

QMutex mutex;
QString s;        // global, thread shared variable

class NotOkClass: public QThread{
    void run(){
        mutex.lock();
        int r = 5;
        if( r % 2 == 0){
            s = "multiple of two";
            QThread::sleep(5);  // do other things
            mutex.unlock(); // you must unlock on every exit point!
            return;
        }
        if( r % 3 == 0){
            s = "multiple of 3";
            QThread::sleep(5);  // do other things
            mutex.unlock(); // you must unlock on every exit point!
            return;
        }
        if( r % 5 == 0){
            s = "multiple of 5";
            QThread::sleep(5);  // do other things
            return;         // woh! you forgot one!
        }
        mutex.unlock();
    }
};

class Writter: public QThread{
    void run(){
        QThread::sleep(1);
        qInfo() << "s = " << s;
        mutex.lock();
        s = "";
        qInfo() << "s = " << s;
        mutex.unlock();
    }
};

int main(int argc, char *argv[])
{
    NotOkClass *not_ok = new NotOkClass();
    Writter *ok = new Writter();

    not_ok->start();
    ok->start();

    not_ok->wait();
    ok->wait();

}
