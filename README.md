# Repo... #

ERASMUS+ classes

Qt: the free cross-platform application framework
March 4-7th, 2017 
Tomas Bata University 
Zlín, Czech Republic 



### How do I get set up? ###

* Install Qt Creator
* Create a new project and select Import project  > Git Clone 
    * repository:  https://pcardoso@bitbucket.org/pcardoso/qt-classes.git
    * path: select a folder to store the repository in your PC



### Who do I talk to? ###

* Pedro Cardoso	(pcardoso@ualg.pt)