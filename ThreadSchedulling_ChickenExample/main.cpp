#include <QDebug>
#include<QThread>

#include <QDebug>
#include<QThread>

// Thread class that... prints a word!
class Writter : public QThread{
public:
    Writter(QString word):QThread(){
        this->word = word;      // receives a Qstring on construction
    }
private:
    QString word;

    void run(){
        qInfo() << word;        // and the prints it on run/start
    }
};


int main(int argc, char *argv[]){
    qInfo() << "Why did the multithreaded chicken cross the road?";

    QList<Writter*> writters;

    foreach (QString word, QString("To get to the other side").split(" ")) {
        Writter *w = new Writter(word);   // send each word to
        w->start();                       // a different thread
        writters.append(w);
    }

    foreach (Writter *w, writters) {
        w->wait();                // wait for all threads to finish
    }

}
