#include "countdown.h"

int main(int argc, char *argv[])
{
    QList<Countdown*> listOfThreads;

    for(int id = 0; id < 10; id++){
        // create and start each of the 10 threads
        Countdown *countdownThread = new Countdown(10);
        countdownThread->start();
        listOfThreads.append(countdownThread);
    }

    // now, wait for them all to finish
    foreach (Countdown *contdowndThread, listOfThreads) {
        contdowndThread->wait();
    }
}
