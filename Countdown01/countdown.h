#ifndef COUNTDOWN_H
#define COUNTDOWN_H

#include <QThread>
#include <QDebug>

class Countdown : public QThread{

public:
     Countdown(int n): QThread(){
         this->start_at = n;
     };

private:
    int start_at;
    void run(){
         int counter = start_at;
         while (counter){
             qInfo("[%d]: %d", this->currentThreadId(), counter--);
             QThread::sleep(1);
         }
         qInfo("[%d]: Over and out!", this->currentThreadId());
     }
};

#endif // COUNTDOWN_H
