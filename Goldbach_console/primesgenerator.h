#ifndef PRIMESGENERATOR_H
#define PRIMESGENERATOR_H

#include <QThread>
#include <QList>
#include <QDebug>

/**
 * @brief The PrimesGenerator class generates primes up to prime larger than a given value
 */
class PrimesGenerator : public QThread
{
    Q_OBJECT
private:

    void run(); /// Reimplementation of Thread::run()

    int upTo;   /// primes generated up to on larger than 'upTo'
    QList<int> primesList;

public:
    PrimesGenerator(int upTo);

signals:
    // declaration of the signal emited every time a new prime number is found

    void newPrime(int p);
};

#endif // PRIMESGENERATOR_H
