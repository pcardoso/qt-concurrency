#include "goldbachworker.h"


GoldbachWorker::GoldbachWorker(int upto):
        QThread(){
    this->upto = upto;
}


void GoldbachWorker::run(){
    for(qint64 value = 4; value <= upto;  value += 2){
        // wait until (the possibly) required primes where computed
        while(largestPrime < value){
            qDebug() << "******** waiting for larger primes!";
            QThread::msleep(100);
        }

        // look for ALL primes of the 'primesList' which sum 'value'
        bool found = false;
        for(auto p1 = primesList.begin(); *p1 < value; p1++){
            for(auto p2 = QList<int>::iterator(p1) ; *p2 < value; p2++){
                if (value == (*p1 + *p2)){
                    qDebug() << value << "==" << *p1 << "+" << *p2;
                    found = true;
                }
            }
        }
        if(!found)
            qDebug() << value << "is not the sum of two primes!";
    }
    emit done();    // emit signal telling that all's done
}

void GoldbachWorker::newPrime(int p){
    qDebug() <<  "worker got " << p;
    primesList.append(p);
    largestPrime = p;
}
