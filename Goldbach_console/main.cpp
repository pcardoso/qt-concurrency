#include "primesgenerator.h"
#include "goldbachworker.h"
#include <QCoreApplication>
#include <QList>


int testUpTo = 100;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    // create the primesGenerator and Golbach "workers"
    PrimesGenerator *PG = new PrimesGenerator(testUpTo);
    GoldbachWorker *worker = new GoldbachWorker(testUpTo);

    // connect them in order to send the primes via signal/slots
    QObject::connect(PG, &PrimesGenerator::newPrime,
                     worker, &GoldbachWorker::newPrime);

    // as usual, start the "workers"
    worker->start();
    PG->start();

    // use the QCoreApplication eventloop which will allow the signal/slots communications
    // to know when it's all done
    QObject::connect(worker, &GoldbachWorker::done, &app, &QCoreApplication::quit);
    app.exec();

    qDebug() << "Over & out!";
}
