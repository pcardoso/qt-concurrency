#ifndef GOLDBACHWORKER_H
#define GOLDBACHWORKER_H

#include <QThread>
#include <QList>
#include <QDebug>

class GoldbachWorker : public QThread
{
    Q_OBJECT
    void run();     // reimplementation of the QThread::run()
    int largestPrime = -1;
    int upto;
    QList<int> primesList;

public:
    GoldbachWorker(int upto);

signals:
    void done();

public slots:
    void newPrime(int p);
};

#endif // GOLDBACHWORKER_H
