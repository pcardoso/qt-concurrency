#include "primesgenerator.h"

PrimesGenerator::PrimesGenerator(int upTo): QThread(){
    this->upTo = upTo;
}

void PrimesGenerator::run(){
    primesList.append(2);
    emit newPrime((int)2);   // emit the first prime
    qDebug() << "new prime" << 2;

    bool stop = false;
    int p = 3;
    while ( !stop ){ // to guarantee a prime larger than N
        foreach (int prime, primesList) {
            if( p % prime == 0)   break;
            if( prime > sqrt(p)){       // every time you find a new
                primesList.append(p);   // new prime, emit it
                emit newPrime(p);
                qDebug() << "new prime" << p;

                // if p is a prime larger than upTo: stop
                if(p > this->upTo) stop = true;

                // stop the foreach
                break;
            }

        }
        p += 2;
    }
}
