#include <QCoreApplication>
#include <QList>
#include <QDebug>
#include <QPair>
#include <QThread>
#include <QFuture>
#include <QtConcurrent/QtConcurrentMap>

//add QT += concurrent to .pro


typedef QPair<int, int> PrimesPair;
typedef QList<PrimesPair> ListOfPrimesPair;
typedef QPair<int, ListOfPrimesPair>  Pair_Even_ListOfPrimesPair;
typedef QMap<int, ListOfPrimesPair>  Map_Even_ListOfPrimesPair;


QList<int> primesList;
int largestPrime = -1;
int upTo = 50;


/*!
 * The PrimesGenerator class runs a thread to compute primes to @param upTo
 * The prime values are stored in the global @param primesList, of type QList<int>.
 * Every time it computes a new prime, the value of @param largestPrime is updated.
 *
 */
class PrimesGenerator: public QThread{
    /**
     * The starting point for the thread. After calling start(),
     * the newly created thread calls this function.
     */
    void run(){
        primesList.append(2);

        bool stop = false;
        int value = 3;
        while ( !stop ){ // to guarantee a prime larger than N
            foreach (int prime, primesList) {
                // if dividable by a smaller prime, it is not a prime
                if( value % prime == 0){
                    break;
                }else{
                    // it is enought to test up to sqrt(value)
                    if( prime > sqrt(value)){
                        // it's a prime. Update primesList and largestPrime
                        primesList.append(value);
                        largestPrime = value;

                        // if value is a prime larger than upTo: stop
                        if(value > upTo)
                            stop = true;

                        // stop foreach
                        break;
                    }
                }
            }
            // test only odd numbers
            value += 2;
        }
    }
};


/**
 * @brief goldbachWorker4Map finds the combinations of primes such that
 * their sum equals @param value and prints it
 * @param value - even number to be tested
 */
void goldbachWorker4Map(int value){

    // wait until (the possibly) required primes where computed
    while(largestPrime < value){
        qDebug() << "[goldbachWorker]: waiting for larger primes!";
        QThread::msleep(100);
    }

    // look for ALL primes of the 'primesList' which sum 'value'
    for(auto p1 = primesList.begin(); *p1 < value; p1++){
        for(auto p2 = QList<int>::iterator(p1) ; *p2 < value; p2++){
            if (value == (*p1 + *p2)){
                qDebug() << value << "==" << *p1 << "+" << *p2;
            }
        }
    }
}



/**
 * @brief goldbachWorker finds the combinations of such that
 * their sum equals @param value
 * @param value - even number to be tested
 * @return a list a pair (value, [(p1,p2), (p1',p2'), ... ]
 */
Pair_Even_ListOfPrimesPair goldbachWorker4MapReduced(int value){

    ListOfPrimesPair pairsList;

    // wait until (the possibly) required primes where computed
    while(largestPrime < value){
        qDebug() << "[goldbachWorker]: waiting for larger primes!";
        QThread::msleep(100);
    }

    // look for ALL primes of the 'primesList' which sum 'value'
    for(auto p1 = primesList.begin(); *p1 < value; p1++){
        for(auto p2 = QList<int>::iterator(p1) ; *p2 < value; p2++){
            if (value == (*p1 + *p2)){
                pairsList.append(PrimesPair(*p1, *p2));
            }
        }
    }
    Pair_Even_ListOfPrimesPair solution;
    solution.first = value;
    solution.second = pairsList;
    return solution;
}

/**
 * joinResults is the reduce function. Receives a solution and joins it to
 *          a final list, returned by blockingMappedReduced
 * @param finalList - aggregation variable
 * @param singleSolution - solution returned by the map phase
 */
void joinResults(Map_Even_ListOfPrimesPair &finalList, Pair_Even_ListOfPrimesPair singleSolution){
    finalList.insert(singleSolution.first, singleSolution.second);
}




int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    PrimesGenerator *PG = new PrimesGenerator();
    PG->start();

    QList<int> valuesList;
    for(int i = 4; i <= upTo; i += 2){
        valuesList.append(i);
    }

    // Map
    // QtConcurrent::blockingMap - will wait for the results. See also QtConcurrent::Map
    QtConcurrent::blockingMap(valuesList, goldbachWorker4Map);

//    // Map & Reduce
//    // QtConcurrent::blockingMappedReduced - will wait for the results. See also QtConcurrent::MappedReduced
//    Map_Even_ListOfPrimesPair result= QtConcurrent::blockingMappedReduced(valuesList, goldbachWorker4MapReduced, joinResults);

//    foreach (int even, result.keys()) {
//        qDebug() << even << result[even] ;
//    }

//    return a.exec();
}
