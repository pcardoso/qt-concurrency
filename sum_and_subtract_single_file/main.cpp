#include <QDebug>
#include <QTime>
#include <QThread>

qint64 value, upto = pow(10, 8);   // global variables

class Decrement : public QThread
{
    void run(){ // decrement 'value' 'upto' times
        for(qint64 i = 0; i < upto; ++i){ value--; }
    }
};

class Increment : public QThread
{
    void run(){ // increment 'value' 'upto' times
        for(qint64 i = 0; i < upto; ++i){ value++; }
    }
};


int main(int argc, char *argv[])
{
    QTime timer;
    timer.start();

    Increment *inc = new Increment();
    Decrement *dec = new Decrement();

    inc->start();         dec->start();
    inc->wait();          dec->wait();

    qInfo() << value << "\ntook " << timer.elapsed() / 1000. << "s";
}

