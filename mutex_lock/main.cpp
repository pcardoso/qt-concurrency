#include <QThread>
#include <QMutex>
#include <QMutexLocker>
#include <QDebug>

QMutex mutex;
QString s;        // global, thread shared variable

class OkClass: public QThread{
    void run(){
        QMutexLocker locker(&mutex);
        int r = 5;
        if( r % 2 == 0){
            s = "multiple of two";
            QThread::sleep(5);
            return;
        }
        if( r % 3 == 0){
            s = "multiple of 3";
            QThread::sleep(5);
            return;
        }
        if( r % 5 == 0){
            s = "multiple of 5";
            QThread::sleep(5);
            return;
        }
    }
};

class Writter: public QThread{
    void run(){
        QThread::sleep(1);
        qInfo() << "s = " << s;
        mutex.lock();
        s = "";
        qInfo() << "s = " << s;
        mutex.unlock();
    }
};

int main(int argc, char *argv[])
{
    OkClass *not_ok = new OkClass();
    Writter *ok = new Writter();

    not_ok->start();
    ok->start();

    not_ok->wait();
    ok->wait();

}
