#include <QMutex>
#include <QThread>
#include <QDebug>
#include <QTime>

qint64 value, upto = pow(10, 8); // global 'shared' variables
QMutex mutex;                    // global mutex object

class Decrement : public QThread{
    void run(){
        for(qint64 i = 0; i < upto; i++){
            mutex.lock();      // acquire the lock
            value--;           // enclose the variable you are writing
            mutex.unlock();    // release the lock
        }
    }
};

class Increment : public QThread{
    void run(){
        for(qint64 i = 0; i < upto; i++){
            mutex.lock();      // acquire the lock
            value++;           // enclose the variable you are writing
            mutex.unlock();    // release the lock
        }
    }
};

int main(int argc, char *argv[])
{

    QTime timer;
    timer.start();

    Increment *inc = new Increment();
    Decrement *dec = new Decrement();

    inc->start();         dec->start();
    inc->wait();          dec->wait();

    qInfo() << value << "\ntook " << timer.elapsed() / 1000. << "s";
}

